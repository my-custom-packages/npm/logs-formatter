import { Request } from "express";
import winston, { Logger as WinstonLoggerType } from "winston";

enum Levels {
  DEBUG = "debug",
  ERROR = "error",
  INFO = "info",
}

export interface ILogFormatter {
  init(req: any): void;
  debug(message: Error): void;
  error(message: Error): void;
  info(message: Error): void;
}

export type NameService = {
  service: {
    name: string;
  };
};

export type RequestType = {
  method: string;
  body: object;
  rawHeaders: object;
  headers: {
    host: string;
  };
};

export default class LogFormatter implements ILogFormatter {
  private logger: WinstonLoggerType;
  private req: Request | any;

  constructor(private config: NameService) {
    this.req = null;
    this.logger = winston.createLogger({
      format: winston.format.combine(
        winston.format.timestamp({
          format: "DD/MM/YYYY, HH:mm:ss",
        }),

        winston.format.prettyPrint(),
        winston.format.errors({ stack: true }),
        winston.format.splat(),
        winston.format.json(),
        winston.format.colorize()
      ),
      defaultMeta: {},
      transports: [
        new winston.transports.Console(),
        new winston.transports.File({
          filename: `logs/${Levels.DEBUG}.log`,
          level: Levels.DEBUG,
        }),
        new winston.transports.File({
          filename: `logs/${Levels.ERROR}.log`,
          level: Levels.ERROR,
        }),
        new winston.transports.File({
          filename: `logs/${Levels.INFO}.log`,
          level: Levels.INFO,
        }),
      ],
    });
  }

  private addMetadata() {
    const req = this.req as RequestType;
    this.logger.defaultMeta = {
      host: req?.headers?.host,
      service: this.config.service.name,
      info: {
        device: req?.rawHeaders,
        method: req?.method,
        body: req?.body,
      },
    };
  }

  private formatInArray(error: Error) {
    if (Array.isArray(error)) {
      error.message = "";
      error.map((e) => {
        for (const [key, value] of Object.entries(e.constraints ?? {})) {
          error.message += `${key}: ${value}. `;
        }
      });
    }
  }

  private formatLogs(error: Error) {
    this.formatInArray(error);
    this.addMetadata();
  }

  init(req: any): void {
    this.req = req;
  }

  debug(error: Error) {
    this.formatLogs(error);
    const message = (error as Error).message;
    this.logger.debug(message);
  }

  error(error: Error) {
    this.formatLogs(error);
    const message = (error as Error).message;
    this.logger.error(message);
  }

  info(error: Error) {
    const message = (error as Error).message;
    this.addMetadata();
    this.logger.info(message);
  }
}
